from typing import Tuple


def hsv_to_rgb(h: float, s: float, v: float) -> Tuple[float, float, float]:
    """
    Not implemented in sim.
    """
    return (0.0, 0.0, 0.0)


def rgb_to_hsv(r: float, g: float, b: float) -> Tuple[float, float, float]:
    """
    Not implemented in sim.
    """
    return (0.0, 0.0, 0.0)
